# Introduction

Go-Blog was created as a task to learn about go.

# Environment setup

* Production 
    * .env
* Development
    * .env.development.local
* Test
    * .env.test

.env.example
```shell
# Application port
APP_BASE_URL = url.dev
APP_PORT = 8080

# Database settings
DB_USER = user
DB_PASSWORD = password
DB_TABLE = table
DB_URL = localhost
DB_PORT = 3306
```

# Running the server

```sh
go run !(*_test).go
```

Additional flags
```sh
go run !(*_test).go envSet -env=production -seed=true -migrate=true
```
**envSet** is a flagSet which holds additional flags that can be set
#### envSet flags
* env - production|development|test - by default, the env is set to development
* migrate - true|false, by default set to false. Used for auto migrate database
* seed - true|false, by default set to false. Used for seeding database with preset data. Can only be used if **migrate** flag is set to true

# Running tests

Running tests from the root directory
This runs all tests in the project
-p=1 is used to run tests one by one so that the test database would not be dropped/edited by another test.

```sh
go test -p=1 ./...
```

# Endpoints

```shell
# GET
# lists support url params: page, limit, order
# and a combination of all of them
/v1/status #returns app status
/v1/articles #returns a list of articles
/v1/article/:id #returns an article by id
/v1/article/:id/comments #returns a list of comments
# POST
/v1/comment/save #saves/edits a comment to an article
/v1/article/save #saves/edits an article
# DELETE
/v1/article/delete/:id #deletes an article by id
/v1/comment/delete/:id #deletes a comment by id
```
A valid request needs to provide **api_key** in the header for authorization (except **/v1/status**).

# Modules used
* [GoDotEnv](https://github.com/joho/godotenv) v1.4.0
* [Gin](https://github.com/gin-gonic/gin) v1.7.7
* [Testify](https://github.com/stretchr/testify) v1.7.0
* [Go Networking](https://pkg.go.dev/golang.org/x/net) v0.0.0-20211216030914-fe4d6282115f
* [GORM/Mysql](https://gorm.io/) v1.2.1
* [GORM](https://gorm.io/) v1.22.4

# TODO
### Endpoints
* comments
    - [X] \(optional) edit comment
    - [X] \(optional) remove comment
* articles
    - [X] \(optional) save article
    - [X] \(optional) edit article
    - [X] \(optional)remove article
- [ ] \(optional) User registration
- [ ] \(optional) User login

### Backend functionality
- [X] \(optional) possibility to enable/disable migration|seed
- [X] \(optional) switch httprouter to gin

### React frontend
- [ ] \(optional) create a react frontend app
